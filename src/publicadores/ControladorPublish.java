/**
 * ControladorPublish.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface ControladorPublish extends java.rmi.Remote {
    public boolean altaVideo(java.lang.String arg0, publicadores.DtVideo arg1) throws java.rmi.RemoteException;
    public boolean altaUsuario(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5, java.lang.String arg6, java.lang.String arg7, boolean arg8, java.lang.String arg9) throws java.rmi.RemoteException;
    public publicadores.DtUsuario iniciarSesion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean existeCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtUsuario buscarUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtCanal retornarDtCanal() throws java.rmi.RemoteException;
    public java.lang.String[] listarSeguidores(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarSeguidos(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtVideo seleccionarVideo(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean seguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean agregarCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listaUsuario() throws java.rmi.RemoteException;
    public java.lang.String[] listarVideos(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listaLike(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String generarIdImagen() throws java.rmi.RemoteException;
    public void modificarUsuario(publicadores.DtUsuario arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listaDeElementos(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.Categoria[] listarCategorias() throws java.rmi.RemoteException;
    public void altaRespuesta(java.lang.String arg0, java.util.Calendar arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public boolean agregarUsuario(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, java.lang.String arg5, java.lang.String arg6, boolean arg7, java.lang.String arg8) throws java.rmi.RemoteException;
    public java.lang.String[] listaDislike(java.lang.String arg0) throws java.rmi.RemoteException;
    public void modificarLista(java.lang.String arg0, publicadores.DtListaDeReproduccion arg1) throws java.rmi.RemoteException;
    public boolean eliminarUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public void modificarVideo(java.lang.String arg0, publicadores.DtVideo arg1) throws java.rmi.RemoteException;
    public java.lang.String[] listaDeCategorias() throws java.rmi.RemoteException;
    public boolean crearListaPorDefecto(java.lang.String arg0) throws java.rmi.RemoteException;
    public void seleccionarComentarioRespuesta(int arg0) throws java.rmi.RemoteException;
    public void modificarUsuarioPassword(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5) throws java.rmi.RemoteException;
    public java.lang.String mostrarDuenioVideo(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtListaDeReproduccion seleccionarListaDeReproduccion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void seleccionarNickname(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtVideo seleccionarVideoLista(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtComentario[] retornarRespuestas(int arg0) throws java.rmi.RemoteException;
    public boolean quitarVideoDeListaReproduccion(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public boolean agregarVideoAListaReproduccion(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public boolean dejarDeSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void ingresarDatosComentario(java.lang.String arg0, java.util.Calendar arg1) throws java.rmi.RemoteException;
    public boolean crearListaparticular(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public void seleccionarVideoConDuenio(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void seleccionarVideo2(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarVideosDeLista(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public java.lang.String[] listarListaDeReproduccion(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarVideosDeListaReproduccion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void agregarComentario(java.lang.String arg0, java.util.Calendar arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public void agregarValoracion(java.lang.String arg0, publicadores.Like arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public publicadores.DtComentario[] seleccionarVideoComentario(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String mostrarDuenioLista(java.lang.String arg0) throws java.rmi.RemoteException;
    public void modificarUsuarioCanal(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, boolean arg3) throws java.rmi.RemoteException;
}
