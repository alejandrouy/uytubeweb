package publicadores;

public class ControladorPublishProxy implements publicadores.ControladorPublish {
  private String _endpoint = null;
  private publicadores.ControladorPublish controladorPublish = null;
  
  public ControladorPublishProxy() {
    _initControladorPublishProxy();
  }
  
  public ControladorPublishProxy(String endpoint) {
    _endpoint = endpoint;
    _initControladorPublishProxy();
  }
  
  private void _initControladorPublishProxy() {
    try {
      controladorPublish = (new publicadores.ControladorPublishServiceLocator()).getControladorPublishPort();
      if (controladorPublish != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)controladorPublish)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)controladorPublish)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (controladorPublish != null)
      ((javax.xml.rpc.Stub)controladorPublish)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public publicadores.ControladorPublish getControladorPublish() {
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish;
  }
  
  public boolean altaVideo(java.lang.String arg0, publicadores.DtVideo arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.altaVideo(arg0, arg1);
  }
  
  public boolean altaUsuario(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5, java.lang.String arg6, java.lang.String arg7, boolean arg8, java.lang.String arg9) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.altaUsuario(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
  }
  
  public publicadores.DtUsuario iniciarSesion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.iniciarSesion(arg0, arg1);
  }
  
  public boolean existeCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.existeCategoria(arg0);
  }
  
  public publicadores.DtUsuario buscarUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.buscarUsuario(arg0);
  }
  
  public publicadores.DtCanal retornarDtCanal() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.retornarDtCanal();
  }
  
  public java.lang.String[] listarSeguidores(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarSeguidores(arg0);
  }
  
  public java.lang.String[] listarSeguidos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarSeguidos(arg0);
  }
  
  public publicadores.DtVideo seleccionarVideo(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarVideo(arg0, arg1);
  }
  
  public boolean seguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seguirUsuario(arg0, arg1);
  }
  
  public boolean agregarCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.agregarCategoria(arg0);
  }
  
  public java.lang.String[] listaUsuario() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listaUsuario();
  }
  
  public java.lang.String[] listarVideos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarVideos(arg0);
  }
  
  public java.lang.String[] listaLike(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listaLike(arg0);
  }
  
  public java.lang.String generarIdImagen() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.generarIdImagen();
  }
  
  public void modificarUsuario(publicadores.DtUsuario arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.modificarUsuario(arg0);
  }
  
  public java.lang.String[] listaDeElementos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listaDeElementos(arg0);
  }
  
  public publicadores.Categoria[] listarCategorias() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarCategorias();
  }
  
  public void altaRespuesta(java.lang.String arg0, java.util.Calendar arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.altaRespuesta(arg0, arg1, arg2);
  }
  
  public boolean agregarUsuario(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, java.lang.String arg5, java.lang.String arg6, boolean arg7, java.lang.String arg8) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.agregarUsuario(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  }
  
  public java.lang.String[] listaDislike(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listaDislike(arg0);
  }
  
  public void modificarLista(java.lang.String arg0, publicadores.DtListaDeReproduccion arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.modificarLista(arg0, arg1);
  }
  
  public boolean eliminarUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.eliminarUsuario(arg0);
  }
  
  public void modificarVideo(java.lang.String arg0, publicadores.DtVideo arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.modificarVideo(arg0, arg1);
  }
  
  public java.lang.String[] listaDeCategorias() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listaDeCategorias();
  }
  
  public boolean crearListaPorDefecto(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.crearListaPorDefecto(arg0);
  }
  
  public void seleccionarComentarioRespuesta(int arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarComentarioRespuesta(arg0);
  }
  
  public void modificarUsuarioPassword(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.modificarUsuarioPassword(arg0, arg1, arg2, arg3, arg4, arg5);
  }
  
  public java.lang.String mostrarDuenioVideo(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.mostrarDuenioVideo(arg0);
  }
  
  public publicadores.DtListaDeReproduccion seleccionarListaDeReproduccion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarListaDeReproduccion(arg0, arg1);
  }
  
  public void seleccionarNickname(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarNickname(arg0);
  }
  
  public publicadores.DtVideo seleccionarVideoLista(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarVideoLista(arg0);
  }
  
  public publicadores.DtComentario[] retornarRespuestas(int arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.retornarRespuestas(arg0);
  }
  
  public boolean quitarVideoDeListaReproduccion(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.quitarVideoDeListaReproduccion(arg0, arg1, arg2);
  }
  
  public boolean agregarVideoAListaReproduccion(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.agregarVideoAListaReproduccion(arg0, arg1, arg2, arg3);
  }
  
  public boolean dejarDeSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.dejarDeSeguirUsuario(arg0, arg1);
  }
  
  public void ingresarDatosComentario(java.lang.String arg0, java.util.Calendar arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.ingresarDatosComentario(arg0, arg1);
  }
  
  public boolean crearListaparticular(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.crearListaparticular(arg0, arg1, arg2, arg3);
  }
  
  public void seleccionarVideoConDuenio(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarVideoConDuenio(arg0, arg1);
  }
  
  public void seleccionarVideo2(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarVideo2(arg0);
  }
  
  public java.lang.String[] listarVideosDeLista(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarVideosDeLista(arg0, arg1);
  }
  
  public java.lang.String[] listarListaDeReproduccion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarListaDeReproduccion(arg0);
  }
  
  public java.lang.String[] listarVideosDeListaReproduccion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarVideosDeListaReproduccion(arg0, arg1);
  }
  
  public void agregarComentario(java.lang.String arg0, java.util.Calendar arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.agregarComentario(arg0, arg1, arg2);
  }
  
  public void agregarValoracion(java.lang.String arg0, publicadores.Like arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.agregarValoracion(arg0, arg1, arg2);
  }
  
  public publicadores.DtComentario[] seleccionarVideoComentario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarVideoComentario(arg0);
  }
  
  public java.lang.String mostrarDuenioLista(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.mostrarDuenioLista(arg0);
  }
  
  public void modificarUsuarioCanal(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, boolean arg3) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.modificarUsuarioCanal(arg0, arg1, arg2, arg3);
  }
  
  
}