package servlets;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtUsuario;

/**
 * Servlet implementation class ModificarDatos
 */
@WebServlet("/ModificarDatos")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)
public class ModificarDatos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarDatos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			
			HttpSession ss= request.getSession(true);
			String user = (String) ss.getAttribute("mailu");
			DtUsuario dtu = port.buscarUsuario(user);
			
			if (dtu!=null) {
				String nombre = request.getParameter("nombre");
				String apellido = request.getParameter("apellido");
				String password = request.getParameter("password");
				String estadoCanal = request.getParameter("si");
				String descripcion = request.getParameter("descripcion");
				String nombreCanal = request.getParameter("canal");
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Calendar cal = Calendar.getInstance();
				try {
					java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
					cal.setTime(date);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String IdImagen= port.generarIdImagen();
				//Nacho
				//String savePath = File.separator + "C:/Users/Ignacio/Desktop/Pap/uytubeweb/WebContent/img";
				//Ale windows
				String savePath = File.separator + "C:/Users/Alejandro/Desktop/uyweb/uytubeweb/WebContent/img";
		        File fileSaveDir=new File(savePath);
		        if(!fileSaveDir.exists()){
		            fileSaveDir.mkdir();
		        }
	
		        System.out.println("IMAGENNNN: " + request.getParameter("imagen"));
			    Part part=request.getPart("imagen");
			    String fileName=extractFileName(part);
			    part.write(savePath + File.separator +IdImagen+ fileName);
			    String filePath= savePath + File.separator +IdImagen+ fileName ;
			    String urlImagen="img/"+IdImagen+fileName;
			    
			    System.out.println("filePath :"+filePath);
			    System.out.println("urlImagen: "+urlImagen);
				if(estadoCanal!=null) {
					port.modificarUsuarioPassword(user, password, nombre, apellido, urlImagen, cal);
					port.modificarUsuarioCanal(user, nombreCanal, descripcion, true);
				}else {
					port.modificarUsuarioPassword(user, password, nombre, apellido, urlImagen, cal);
					port.modificarUsuarioCanal(user, nombreCanal, descripcion, false);
				}						
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("/index.jsp");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
}
	
	private String extractFileName(Part part) {
	    String contentDisp = part.getHeader("content-disposition");
	    String[] items = contentDisp.split(";");
	    for (String s : items) {
	        if (s.trim().startsWith("filename")) {
	            return s.substring(s.indexOf("=") + 2, s.length()-1);
	        }
	    }
	    return "";
	}
}