package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class DejarSeguir
 */
@WebServlet("/DejarSeguir")
public class DejarSeguir extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DejarSeguir() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
					
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			HttpSession htps=request.getSession();
			String dejado= request.getParameter("usuario");
			String usuarioCuenta= (String)htps.getAttribute("mailu");
			System.out.println("Voy a dejar de seguir a: "+dejado);
			System.out.println("Con: "+usuarioCuenta);
			port.dejarDeSeguirUsuario(usuarioCuenta, dejado);
			RequestDispatcher rd;
			//rd = request.getRequestDispatcher("/verSeguidores.jsp?nick="+usuarioCuenta);
			rd = request.getRequestDispatcher("/verUsuario.jsp?id="+usuarioCuenta);
			rd.forward(request, response);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
