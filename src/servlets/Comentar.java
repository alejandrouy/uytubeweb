package servlets;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class Comentar
 */
@WebServlet("/Comentar")
public class Comentar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Comentar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try {
			
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			String Comentario=request.getParameter("comentario");
			Date fecha=new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(fecha);
			HttpSession sesion=request.getSession(false);
			String usuarioDuenio=request.getParameter("Usuario");
			String video=request.getParameter("Video");
			if(sesion!=null) {
				String usuario=(String) sesion.getAttribute("mailu");
				System.out.println("usuario:"+usuario);
				
				
				port.seleccionarVideoConDuenio(video,usuarioDuenio);;
				port.agregarComentario(Comentario,cal,usuario);
			}
			
			  RequestDispatcher rd; 
			  rd = request.getRequestDispatcher("VerVideo.jsp?id="+video+"&id2="+usuarioDuenio);
			  rd.forward(request, response);
			} catch (Exception ioe) {
	            ioe.printStackTrace();
	            
	        }
		
	}

}
