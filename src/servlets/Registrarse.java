package servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;


/**
 * Servlet implementation class Registrarse
 */
@WebServlet("/Registrarse")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
		maxFileSize=1024*1024*10,      // 10MB
		maxRequestSize=1024*1024*50)
public class Registrarse extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String path;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registrarse() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String nickname = request.getParameter("nickname");
		String email = request.getParameter("email");
		String canal = request.getParameter("canal");
		String password = request.getParameter("password");
		String cpassword = request.getParameter("confirm-password");
		String descripcion = request.getParameter("descripcion");
		String esPrivado = request.getParameter("si");
		String imagen = request.getParameter("imagen");
		Date fechaNac = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		/*try {
			System.out.println("ESTOY EN EL TRYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");
			Date date = (Date)sdf.parse(request.getParameter("date")); 
			System.out.println("Fecha en el date: " + date);
			cal.setTime(date);
			System.out.println("Fecha en el calendar: " + cal);

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			System.out.println("ESTOY EN EL CATCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		}*/
		
		try {
			java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
			cal.setTime(date);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			//esto depende de donde tengas el proyecto
//			String rutaImagenes="C:/Users/Javi/Desktop/PAP/uytubeweb/WebContent/img/imagen"+IdImagen+".jpg";
//			//////////////****************//////////////////////////////////////////////
//			boolean copiaImagen=copyFile(imagen,rutaImagenes);
			
			String IdImagen= port.generarIdImagen();
			//javo
			//String savePath = File.separator + "C:/Users/Javi/Desktop/TEC/WEB/uytubeweb/WebContent/img"; //specify your path here
		
			//Compu grande de Gaston
			//String savePath = File.separator + "C:/Users/gasto/OneDrive/Escritorio/uytubeweb/WebContent/img";
			// Compu chica de gaston
			//String savePath = File.separator + "C:/Users/Gast�n L�pez/Desktop/uytubeweb/WebContent/img";
			
			//Ale windows
			String savePath = File.separator + "C:/Users/Alejandro/Desktop/uyweb/uytubeweb/WebContent/img";
			//ale linux
			//String savePath = File.separator + "home/alejandro/Desktop/uytuveweb/uytubeweb/WebContent/img"; //specify your path here
	        File fileSaveDir=new File(savePath);
	        if(!fileSaveDir.exists()){
	            fileSaveDir.mkdir();
	        }

		    Part part=request.getPart("imagen");
		    String fileName=extractFileName(part);
		    
		    part.write(savePath + File.separator +IdImagen+ fileName);
			
		    String filePath= savePath + File.separator +IdImagen+ fileName ;
		    
		    String urlImagen="img/"+IdImagen+fileName;
		    System.out.println("filePath :"+filePath);
		    System.out.println("urlImagen: "+urlImagen);
	    	boolean ok;
	    		if(esPrivado!=null) {
	    			System.out.println("ESTOY ACAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2222222222222222");
					ok = port.altaUsuario(nickname, password, nombre, apellido, email, cal, urlImagen, canal, true, descripcion);
					if(ok) {
						HttpSession sesion=request.getSession(); 
						sesion.setAttribute("mailu", nickname);
						RequestDispatcher rd; rd = request.getRequestDispatcher("/index.jsp");
						rd.forward(request, response);
					}else {
						RequestDispatcher rd;
						rd = request.getRequestDispatcher("/notificacion2.jsp");
						rd.forward(request, response);
					}
				}else {
					System.out.println("ESTOY ACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
					ok = port.altaUsuario(nickname, password, nombre, apellido, email, cal, urlImagen, canal, false, descripcion);
					if(ok) {
						HttpSession sesion=request.getSession(); 
						sesion.setAttribute("mailu", nickname);
						RequestDispatcher rd; rd = request.getRequestDispatcher("/index.jsp");
						rd.forward(request, response);
					}else {
						RequestDispatcher rd;
						rd = request.getRequestDispatcher("/notificacion2.jsp");
						rd.forward(request, response);
					}
				}
			/*
			 * if((checkUserNick(nickname)) && (checkUserMail(email))){
			 * System.out.println("Entre al primer if"); RequestDispatcher rd; rd =
			 * request.getRequestDispatcher("/notificacion2.jsp"); rd.forward(request,
			 * response); }else if((!checkUserNick(nickname)) && (checkUserMail(email))){
			 * System.out.println("Entre al segundo if"); RequestDispatcher rd; rd =
			 * request.getRequestDispatcher("/notificacion2.jsp"); rd.forward(request,
			 * response); }else if((checkUserNick(nickname)) && (!checkUserMail(email))){
			 * System.out.println("Entre al tercero if"); RequestDispatcher rd; rd =
			 * request.getRequestDispatcher("/notificacion2.jsp"); rd.forward(request,
			 * response); }else if((!checkUserNick(nickname)) && (!checkUserMail(email))){
			 * System.out.println("Entre al ultimo if"); if(esPrivado!=null) { estaIniciado
			 * = port.altaUsuario(nickname, password, nombre, apellido, email, cal,
			 * urlImagen, canal, true, descripcion); }else { estaIniciado =
			 * port.altaUsuario(nickname, password, nombre, apellido, email, cal, urlImagen,
			 * canal, false, descripcion); }
			 * 
			 * request.setAttribute("estaIniciado", estaIniciado); HttpSession sesion=
			 * request.getSession(); sesion.setAttribute("mailu", nickname);
			 * RequestDispatcher rd; rd = request.getRequestDispatcher("/index.jsp");
			 * rd.forward(request, response); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//		System.out.println("imagen="+imagen);
//		File archivo = new File(imagen);
//		
//		
		
		
	}
	private String extractFileName(Part part) {
	    String contentDisp = part.getHeader("content-disposition");
	    String[] items = contentDisp.split(";");
	    for (String s : items) {
	        if (s.trim().startsWith("filename")) {
	            return s.substring(s.indexOf("=") + 2, s.length()-1);
	        }
	    }
	    return "";
	}
	
	public boolean copyFile(String fromFile, String toFile) {
        File origin = new File(fromFile);
        File destination = new File(toFile);
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                // We use a buffer for the copy (Usamos un buffer para la copia).
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                return true;
        	}catch (IOException ioe) {
                ioe.printStackTrace();
                return false;
            }
        }else {
            return false;
        }
    }
	public boolean checkUserNick(String nickname){
        boolean ret=false;
        Connection con = null;
        //Ale
        String url = "jdbc:postgresql://localhost:5432/uytube";
        String bduser = "postgres";
        String pass = "ale";
		try{
			Class.forName("org.postgresql.Driver");
        	con = DriverManager.getConnection(url,bduser,pass);
            
			PreparedStatement ps = con.prepareStatement("SELECT nickname FROM usuarios WHERE nickname = ?");
			ps.setString(1, nickname);

			ResultSet rs = ps.executeQuery();
			ret = rs.next();
			if(ret) {
				System.out.println("Verdadero User: "+nickname);
			}
			else {
				System.out.println("Falso User: "+nickname);
			}
        }catch(Exception e){
            e.printStackTrace();
        }
		return ret;
    }
	
	public boolean checkUserMail(String email){
        boolean ret=false;
        Connection con = null;
        //Ale
        String url = "jdbc:postgresql://localhost:5432/uytube";
        String bduser = "postgres";
        String pass = "ale";
		try{
			Class.forName("org.postgresql.Driver");
        	con = DriverManager.getConnection(url,bduser,pass);
            
			PreparedStatement ps = con.prepareStatement("SELECT email FROM usuarios WHERE email = ?");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			ret = rs.next();
			if(ret) {
				System.out.println("Verdadero Email: "+email);
			}
			else {
				System.out.println("Falso Email: "+email);
			}
        }catch(Exception e){
            e.printStackTrace();
        }
		return ret;
    }
	
	 
}
