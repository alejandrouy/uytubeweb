package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class CrearLista
 */
@WebServlet("/CrearLista")
public class CrearLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CrearLista() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher rd;
		HttpSession ses = request.getSession();
		try{ 
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			String usuario = (String)ses.getAttribute("mailu");
			String nombreLista = request.getParameter("nombre");
			String esPrivada = request.getParameter("si");
			String noPrivada = request.getParameter("no");
			String categoria = request.getParameter("categoria");
			if(esPrivada!=null) {
				port.crearListaparticular(usuario, nombreLista, true, categoria);
			}else {
				port.crearListaparticular(usuario, nombreLista, false, categoria);
			}
			rd = request.getRequestDispatcher("/ConsultaLista.jsp");
			rd.forward(request, response);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
