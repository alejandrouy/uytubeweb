package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.Like;
/**
 * Servlet implementation class ValorarLike
 */
@WebServlet("/ValorarLike")
public class ValorarLike extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ValorarLike() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession htps=request.getSession();
		
		String video=request.getParameter("Video");
		String liked=request.getParameter("Like");
		String usuario = (String) htps.getAttribute("mailu");
		String disliked=request.getParameter("Dislike");
		String usuarioDuenio=request.getParameter("Usuario");
		
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
		
		System.out.println("El video es: "+video);
		System.out.println("Like: "+liked);
		System.out.println("Dislike: "+disliked);
		if(liked!=null) {
			port.agregarValoracion(video,Like.like, usuario);
		}else if(disliked!=null) {
			port.agregarValoracion(video,Like.dislike, usuario);
		}else {
			System.out.println("ERROR ERROR ERROR ERROR WARNING ESTOY EN ValorarLike.java");
		}
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("VerVideo.jsp?id="+video+"&id2="+usuarioDuenio);
		rd.forward(request, response);
		
	} catch (Exception e) {
		e.printStackTrace();
	}
		
		
	}

}
