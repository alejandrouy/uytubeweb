package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class SubirVideo
 */
@WebServlet("/SubirVideo")
public class SubirVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubirVideo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		HttpSession sesion= request.getSession(true);
		String usuario=(String) sesion.getAttribute("mailu");
		
		String nombreVideo=request.getParameter("NombreVideo");
		String descripcion=request.getParameter("Descripcion");
		String categoria=request.getParameter("categoria");
		String URL=request.getParameter("URL");
		String codigoYoutube="https://www.youtube.com/embed/"+URL.substring(32,43);
		String privado = request.getParameter("si");
		Date fechaNac = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date;
		Calendar cal = Calendar.getInstance();
		
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			
		
		publicadores.DtVideo dtv;
		if(privado!=null) {
			dtv=new publicadores.DtVideo(nombreVideo,true,categoria,descripcion,"0",cal,codigoYoutube);
		}else {
			dtv=new publicadores.DtVideo(nombreVideo,false,categoria,descripcion,"0",cal ,codigoYoutube);
		}
	
		boolean seCreo=port.altaVideo(usuario,dtv);
		
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("/index.jsp");
		rd.forward(request, response);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
