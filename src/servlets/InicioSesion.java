package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*import interfaces.Fabrica;
import interfaces.IControladorUsuario;*/
import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtUsuario;

/**
 * Servlet implementation class InicioSesion
 */
@WebServlet("/InicioSesion")
public class InicioSesion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InicioSesion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String user = request.getParameter("email");
		String password = request.getParameter("password");
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			try {
				DtUsuario dtu=port.iniciarSesion(user, password);
				HttpSession sesion= request.getSession(true);
				sesion.setAttribute("mailu", dtu.getNickname());
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("/index.jsp");
				rd.include(request, response);
			}catch (Exception e) {
				PrintWriter out = response.getWriter();
				out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
				out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
				out.println("<script>");
				out.println("$(document).ready(function(){");
				out.println("swal ( 'Error' , 'Usuario o Password incorrecto!' , 'error' );");
				out.println("});");
				out.println("</script>");
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("/iniciarSesion.jsp");
				rd.include(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public boolean checkUser(String user, String password){
        boolean ret=false;
        Connection con = null;
        //Ale
        String url = "jdbc:postgresql://localhost:5432/uytube";
        String bduser = "postgres";
        String pass = "ale";
        
        //El Resto del equipo
        /*String url = "jdbc:postgresql://localhost:5432/postgres";
        String bduser = "postgres";
        String pass = "1234";*/
		try{
			Class.forName("org.postgresql.Driver");
        	con = DriverManager.getConnection(url,bduser,pass);
            
			PreparedStatement ps = con.prepareStatement("SELECT nickname, contrasenia FROM usuarios WHERE nickname = ? and contrasenia = ?");
			ps.setString(1, user);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			ret = rs.next();
			if(ret) {
				System.out.println("Verdadero User: "+user+" Password: "+password);
			}
			else {
				System.out.println("Falso User: "+user+" Password: "+password);
			}
        }catch(Exception e){
            e.printStackTrace();
        }
		return ret;
    }
	

}
