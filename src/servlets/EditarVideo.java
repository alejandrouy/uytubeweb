package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtVideo;

/**
 * Servlet implementation class EditarVideo
 */
@WebServlet("/EditarVideo")
public class EditarVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditarVideo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		try {			
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			
			HttpSession ss= request.getSession(true);
			String user = (String) ss.getAttribute("mailu");
			String nombrevideoAntes = request.getParameter("nombrevideo");
			String nombreVideo = request.getParameter("nombre");
			String descripcion = request.getParameter("descripcion");
			String URL = request.getParameter("url");
			String codigoYoutube="https://www.youtube.com/embed/"+URL.substring(32,43);
			String estadoCanal = request.getParameter("publico");
			String categoria = request.getParameter("categoria");
			boolean publico = true;
			if("Publico".equals(estadoCanal)) {
				publico = true;
			}else if("Privado".equals(estadoCanal)) {
				publico = false;
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date date=new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			DtVideo dtv = new DtVideo(nombreVideo, publico , categoria, descripcion,"",cal,codigoYoutube);
			port.modificarVideo(nombrevideoAntes, dtv);
			RequestDispatcher rd;
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}