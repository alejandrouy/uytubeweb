package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtListaDeReproduccion;

/**
 * Servlet implementation class ModificarLista
 */
@WebServlet("/ModificarLista")
public class ModificarLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarLista() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{ 
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			RequestDispatcher rd;
			HttpSession ses = request.getSession();
			String usuario = (String)ses.getAttribute("mailu");
			String nombreLista = request.getParameter("nombreLista");
			String esPrivada = request.getParameter("si");
			String noPrivada = request.getParameter("no");
			System.out.println("-----------------------PRIVADA:"+esPrivada);
			System.out.println("-----------------------USUARIO:"+usuario);
			DtListaDeReproduccion dtl= port.seleccionarListaDeReproduccion(nombreLista, usuario);
			System.out.println("-----------------------NOMBRE DE LA LISTA:"+dtl.getNombre());
			if(esPrivada!=null) {
				DtListaDeReproduccion dtl2=new DtListaDeReproduccion(nombreLista, true, dtl.getCategoria());
				port.modificarLista(nombreLista, dtl2);
			}else {
				DtListaDeReproduccion dtl2=new DtListaDeReproduccion(nombreLista, false, dtl.getCategoria());
				port.modificarLista(nombreLista, dtl2);
			}
			rd = request.getRequestDispatcher("/ListaDeReproduccion.jsp?id="+nombreLista);
			rd.forward(request, response);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
