package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class AgregarVideoALista
 */
@WebServlet("/AgregarVideoALaLista")
public class AgregarVideoALaLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregarVideoALaLista() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			HttpSession htps= request.getSession();
			String videoYnickname = request.getParameter("videoSeleccionado");
			String listaSelecc = (String) htps.getAttribute("lista");
			String duenioLista = (String) htps.getAttribute("mailu");
			String[] partes = videoYnickname.split("@");
			String video=partes[0];
			String nickname=partes[1];
			System.out.println("El video a agregar con nickname: "+videoYnickname);
			System.out.println("Solo el video: "+video);
			System.out.println("Solo el nickname: "+nickname);
			System.out.println("A la lista: "+listaSelecc);
			System.out.println("De: "+duenioLista);
			boolean agrego = port.agregarVideoAListaReproduccion(nickname,duenioLista,video,listaSelecc);
			if(agrego) {
				System.out.println("Exito");
			}else {
				System.out.println("Fracaso");
			}
			RequestDispatcher rd;
			htps.setAttribute("nick", nickname);
			request.setAttribute("nick", nickname);
			rd = request.getRequestDispatcher("ListaDeReproduccion.jsp?id="+listaSelecc+"&id2="+duenioLista);
			rd.forward(request, response);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
