<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="ISO-8859-1">
<title>Consulta Videos</title>
 <jsp:include page="header.jsp"/>
</head>
<body>
<div class="list-group" style="margin-top: 150px; margin-left: 50px; margin-right: 50px;">
	<%String usuario=request.getParameter("id"); %>
	<h6>Videos de <%=usuario %></h6>
	<%
	try{
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		

		int i = 0;
		String[] videos = port.listarVideos(usuario);
		if(videos.length>0){
			for(int k=0;k<videos.length;k++){%>
					<a href="VerVideo.jsp?id=<%=videos[k]%>&id2=<%=usuario%>" class="list-group-item list-group-item-primary" type="submit"><%=videos[k]%></a>
				
				<%
				i++;
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}%>
	
</div>

<jsp:include page="footer.jsp" />
</body>
</html>