<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtUsuario"%>
\
<!DOCTYPE html>
<html>
<head>
<jsp:include page="header.jsp"/>
<meta charset="UTF-8">
<title>Modificar Datos</title>
</head>
<body>
<% 
HttpSession ss= request.getSession(true);
String user = (String) ss.getAttribute("mailu");


ControladorPublishService cps = new ControladorPublishServiceLocator();
ControladorPublish port = cps.getControladorPublishPort();
DtUsuario dtu = port.buscarUsuario(user);

%>
<div class="container" id="wrap">
	  <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="ModificarDatos" method="POST" accept-charset="utf-8" class="form" role="form" enctype='multipart/form-data'>   <legend>Modificar Perfil</legend>
              		<div>
              			<label>Nickname</label>
                    	<input type="text" name="nickname" value="" class="form-control input-lg" placeholder="<%=dtu.getNickname() %>" disabled/>
                    </div>
              		<label>Email</label>
                    <input type="text" name="email" value="<%=dtu.getEmail() %>" class="form-control input-lg" placeholder="<%=dtu.getEmail() %>"  disabled/>
                    <label>Nombre</label>
                    <input type="text" name="nombre" value="<%=dtu.getNombre() %>" class="form-control input-lg" placeholder="<%=dtu.getNombre() %>"/>
                    <label>Apellido</label>
                    <input type="text" name="apellido" value="<%=dtu.getApellido() %>" class="form-control input-lg" placeholder="<%=dtu.getApellido() %>"/>        
                    <label>Descripcion</label>
                    <input type="text" name="descripcion" value="" class="form-control input-lg" placeholder="Pudes modificar la descripcion del canal" />
                    <label>Contraseña</label>
                    <input type="password" name="password" value="" class="form-control input-lg" placeholder="Contraseña" required />
                    <label>Confirmar Contraseña</label>
                    <input type="password" name="cpassword" value="" class="form-control input-lg" placeholder="Confirmar Contraseña" required /> 
                    <label>Fecha de Nacimiento</label>  
                    <input type="date" name="date" />
                 
              		<div class="btn-group btn-group-toggle" data-toggle="buttons">Canal privado: 
						<label class="btn btn-secondary active"><input type="radio" name="si" id="si" autocomplete="off"> SI </label>
						<label class="btn btn-secondary active"><input type="radio" name="no" id="no" autocomplete="off"> NO </label>
					</div>
					    <input type="file" name="imagen" accept="image/*" onchange="loadFile(event)">
					<img id="output"/>
					<script>
					  var loadFile = function(event) {
					    var output = document.getElementById('output');
					    output.src = URL.createObjectURL(event.target.files[0]);
					  };
					</script>
    
                    <br />
                    <button class="btn btn-secondary btn-lg btn-block" type="submit">Guardar Cambios</button>
            </form>          
    </div>
</div>
<jsp:include page="footer.jsp"/>

</div>      

</body>
</html>