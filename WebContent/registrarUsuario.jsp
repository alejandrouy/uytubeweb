<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.awt.event.ActionEvent"%>
<%@page import="java.awt.event.ActionListener"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.OutputStream"%>  
    
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Registrarse</title>
<jsp:include page="header.jsp"/>
<style>
</style>

</head>

<br>
<br>
<body>
	<div class="container">
		<div class="row justify-content-center">
		        <div class="col-md-8">
		            <div class="card">
		                <div class="card-header">Registro</div>
		                <div class="card-body">
		                        <form action="Registrarse" method="POST" accept-charset="utf-8" class="form" role="form" enctype="multipart/form-data" onsubmit="return Validate()" name="vform">
		                        <div class="form-group">
		                            <label for="name" class="cols-sm-2 control-label">Nombre</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
		                                    <input type="text" class="form-control" name="nombre" id="" placeholder="Ingrese su nombre" required/>
		                                </div>
		                            </div>
		                            <label for="name" class="cols-sm-2 control-label">Apellido</label>
		                        <div class="form-group">
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
		                                    <input type="text" class="form-control" name="apellido" id="" placeholder="Ingrese su Apellido" required />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="email" class="cols-sm-2 control-label">Email</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
		                                    <input type="text" class="form-control" name="email" id="email" placeholder="Ingrese su Email" required />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="username" class="cols-sm-2 control-label">Nickname</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
		                                    <input type="text" class="form-control" name="nickname" id="" placeholder="Ingrese su nickname" required />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="username" class="cols-sm-2 control-label">Canal</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
		                                    <input type="text" class="form-control" name="canal" id="" placeholder="Ingrese nombre del canal" required />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="username" class="cols-sm-2 control-label">Descripcion del Canal</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
		                                    <input type="text" class="form-control" name="descripcion" id="username" placeholder="Ingrese descripcion del canal" required />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                                <label for="username" class="cols-sm-2 control-label">Tipo de Privacidad</label>
		                                <div class="cols-sm-10">
		                                    <div class="input-group">
		                                        <input type="radio" name="si" id="si" autocomplete="off"> Publico </label>
		                                        <input type="radio" name="no" id="no" autocomplete="off"> Privado </label>
		                                    </div>
		                                </div>
		                            </div>
		                        <div class="form-group">
		                            <label for="password" class="cols-sm-2 control-label">Password</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
		                                    <input type="password" class="form-control" name="password" id="" placeholder="Ingrese su password" required />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="confirm" class="cols-sm-2 control-label">Confirmacion Password</label>
		                            <div class="cols-sm-10">
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
		                                    <input type="password" class="form-control" name="confirm_password" id="confirm" placeholder="Ingrese nuevamente la password" />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                                <label for="confirm" class="cols-sm-2 control-label">Ingrese la fecha</label>
		                                <div class="cols-sm-10">
		                                    <div class="input-group">
		                                        <input type="date" name="date" required />
		                                    </div>
		                                </div>
		                            </div>
		                        <div class="form-group ">
		                            <label for="exampleFormControlFile1">Ingrese la imagen del perfil</label>
		                            <input type="file" name="imagen" accept="image/*" onchange="loadFile(event)">
		                            <script>
		                            var loadFile = function(event) {
		                                var output = document.getElementById('output');
		                                output.src = URL.createObjectURL(event.target.files[0]);
		                            };
		                            </script>
		                            <script type="text/javascript">
									    var nickname = document.forms["vform"]["nickname"];
									    var email = document.forms["vform"]["email"];
									    var password = document.forms["vform"]["password"];
									    var cpassword = document.forms["vform"]["confirm_password"];
									
									    function Validate() {
									    // validate username
									        if (nickname.value == "") {
									            alert("Ingrese un nickname!");
									            nickname.focus();
									            return false;
									        }
									          //  email
									        if (email.value == "") {
									            alert("Ingrese una direccion de correo!");
									            email.focus();
									            return false;
									        }
									        //  password
									        if (password.value == "") {
									            alert("Ingrese una contraseña!");
									            password.focus();
									            return false;
									        }
									        // check if the two passwords match
									        if (password.value != cpassword.value) {
									            alert("Las contraseñas deben ser iguales!");
									            return false;
									        }
									    }
									</script>
		                        </div>
		                        <div class="form-group ">
		                        	<input type="submit" class="btn btn-primary btn-lg btn-block login-button" value="Registrarse">
		                         </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
		    <img class="w3-display-middle w3-large" id="output" style="width:100%;max-width:400px"> 
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</body>
	<br>
	<br>
	<br>
	<br>
</html>


