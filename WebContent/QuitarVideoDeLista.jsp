<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Quitar Video de la lista</title>
<jsp:include page="header.jsp"/>
</head>
<body>
  <%try{ 
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();HttpSession htps=request.getSession();
		String lista=request.getParameter("id");
		String video=request.getParameter("id2");
		String nickLista=(String)htps.getAttribute("mailu");
		boolean quito= port.quitarVideoDeListaReproduccion (video,lista,nickLista);
		if(quito){%>
			<h1><%=video%> se ha borrado de <%=lista %> exitosamente.</h1>
		<%}else{ %>
			<h1><%=video%> no se ha podido borrar de <%=lista %>, instruyase con el manual de usuario del programa o con el desarrollador mas cercano (Con Gast�n principalmente, que es un kpo ;)).</h1>
		<%} %>
		<a href="ListaDeReproduccion.jsp?id=<%=lista %>&id2=<%= nickLista%>" class="btn btn-secondary" type="submit">Volver a la lista</a>
	<%}catch (Exception e) {
		e.printStackTrace();
	  }%>
	<jsp:include page="footer.jsp"/> 
</body>
</html>