<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="java.util.*"%>
 
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Tus Videos</title>
<jsp:include page="header.jsp"/>
</head>
<body>
<div class="list-group" style="margin-top: 100px; margin-left: 50px; margin-right: 50px;">
    <%
    try{
        ControladorPublishService cps = new ControladorPublishServiceLocator();
        ControladorPublish port = cps.getControladorPublishPort();
       
        HttpSession ss= request.getSession(true);
        String user = (String) ss.getAttribute("mailu");
        String[] listarVideos =port.listarVideos(user);
            
        for (int i=0;i<listarVideos.length;i++){
        %>
            <a href="editarVideo.jsp?id=<%=listarVideos[i]%> " class="list-group-item list-group-item-primary" type="submit"><%=listarVideos[i]%></a>
        <%}
   
     }catch(Exception e){
    e.printStackTrace();
}
%>
</div>
<jsp:include page="footer.jsp" />
</body>
</html>