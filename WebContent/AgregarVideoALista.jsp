<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtVideo" %>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head> 
<meta charset="ISO-8859-1">
<title>Agregar un Video</title>
<jsp:include page="header.jsp"/>
</head>
<body>
	<form action="AgregarVideoALaLista" method="post" class="form-signin">
		<div class="list-group" style="margin-top: 200px;">
			<select name="videoSeleccionado">
				<%try{ 
					ControladorPublishService cps = new ControladorPublishServiceLocator();
					ControladorPublish port = cps.getControladorPublishPort();
					HttpSession htps=request.getSession(false);
					String user= (String) htps.getAttribute("mailu");
					String listaSel= request.getParameter("id");
					htps.setAttribute("lista", listaSel);
					String[] listaUsuarios=port.listaUsuario();
					
					for(int i=0;i<listaUsuarios.length;i++){
						System.out.println("Estoy con el usuario: "+listaUsuarios[i]);
						String[] listaVideos=port.listarVideos(listaUsuarios[i]);
						if((listaVideos.length>0) && (listaVideos!=null)){
							for(int j=0;j<listaVideos.length;j++){
								System.out.println("Encontre al video: "+listaVideos[j]);
								if(user.contentEquals(listaUsuarios[i])){%>
									<option value= "<%=listaVideos[j]+"@"+listaUsuarios[i] %>" class="list-group-item list-group-item-dark"><%=listaVideos[j] %> (<%=listaUsuarios[i] %>)</option>
								<%}else{ 
									if(!port.seleccionarVideo(listaVideos[j], listaUsuarios[i]).isPrivado()){%>
									<option value= "<%=listaVideos[j]+"@"+listaUsuarios[i] %>" class="list-group-item list-group-item-dark"><%=listaVideos[j] %> (<%=listaUsuarios[i] %>)</option>								
									<%} %>
								<%} %>
							<%} %>
						<%} %>
					<%}
				}catch (Exception e) {
					e.printStackTrace();
	  			}%>
			</select>
		</div>
		<button class="btn btn-secondary" type="submit">Agregar</button>
	</form>
<jsp:include page="footer.jsp"/>
</body>
</html>