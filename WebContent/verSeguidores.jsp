<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtUsuario"%>
<%@page import="publicadores.DtCanal"%>
<%@ page import ="java.util.ArrayList"%>
<%@ page import ="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="header.jsp"/>
<meta charset="ISO-8859-1">
<title>Seguidores/Seguidos</title>
</head>
<body>
<div class="list-group">
<%try{ 
	ControladorPublishService cps = new ControladorPublishServiceLocator();
	ControladorPublish port = cps.getControladorPublishPort();
	HttpSession ss = request.getSession(true);
	String usuarioCuenta=(String) ss.getAttribute("mailu");
	String user = request.getParameter("nick");
	String[] listaSeguidores = port.listarSeguidores(user);
	String[] listaSeguidos = port.listarSeguidos(user);	


	%>
	<%if(listaSeguidores.length>0){%>
			<div class="list-group" style="margin-top: 50px; margin-left: 100px; margin-right: 100px; margin-bottom: 100px;">
				<p>Seguidores</p>
				<%for(int s=0;s<listaSeguidores.length;s++){%>
					  	<a href="verUsuario.jsp?id=<%=listaSeguidores[s] %>" class="list-group-item list-group-item-primary"><%=listaSeguidores[s] %></a>
				<%} %>  			
			</div>
	<%}%>
	</div>
	<div class="list-group">
	<%if(listaSeguidos.length>0){%>
			<div class="list-group" style="margin-top: 50px; margin-left: 100px; margin-right: 100px; margin-bottom: 100px;">
			
			<p>Siguiendo</p>
			<%for(int s2=0;s2<listaSeguidos.length;s2++){%>			
				  	<a href="verUsuario.jsp?id=<%=listaSeguidos[s2] %>" class="list-group-item list-group-item-primary"><%=listaSeguidos[s2] %></a>
				  	<%if(usuarioCuenta!=null){ 
				  		if(user.contentEquals(usuarioCuenta)){%>
					  	<form action="DejarSeguir" method="post">
					  		<input type="hidden" name="usuario" id="usuario" value="<%=listaSeguidos[s2] %>">
					  		<button class="btn btn-secondary active" type="submit">Dejar de Seguir</button>
					  	</form>
					  	<%}
				  	}%>
			<%}%>
			</div>
	<%}%>
<%}catch (Exception e) {
	e.printStackTrace();
} %>
</div>

<jsp:include page="footer.jsp"/>
</body>
</html>