<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtListaDeReproduccion" %>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista De Reproduccion</title>
<jsp:include page="header.jsp"/>
</head>
<body>
  <%String listaSeleccionada = request.getParameter("id");%>
	<h1><%=listaSeleccionada %> - VIDEOS:</h1>
  <%try{ 
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		HttpSession htps=request.getSession();
		String user= (String) htps.getAttribute("mailu");
		String duenioLista= request.getParameter("id2");
		DtListaDeReproduccion dtl=port.seleccionarListaDeReproduccion(listaSeleccionada, duenioLista);
		String[] videos = port.listarVideosDeLista(listaSeleccionada, duenioLista);

	for(int i=0; i<port.listarVideosDeLista(listaSeleccionada, duenioLista).length;i++){
		
	%>
		<a href="VerVideo.jsp?id=<%=videos[i] %>&id2=<%=port.mostrarDuenioVideo(videos[i]) %>" class="list-group-item list-group-item-dark" type="submit"><%=port.mostrarDuenioVideo(videos[i])%> (<%=videos[i]%>)</a>
		<%if(user.contentEquals(duenioLista)){ %>
			<a href="QuitarVideoDeLista.jsp?id=<%=listaSeleccionada %>&id2=<%=videos[i] %>" class="btn btn-secondary" type="submit">Quitar Video</a>
		<%} %>
	<%}%>
	<%if(user.contentEquals(duenioLista)){ %>
		<div>
			<a href="AgregarVideoALista.jsp?id=<%=listaSeleccionada %>" class="btn btn-secondary btn-lg btn-block" type="submit">Agregar un video a la lista</a>
		</div>
		<div>
			<a href="ModificarLista.jsp?id=<%=listaSeleccionada %>" class="btn btn-secondary btn-lg btn-block" type="submit">Modificar Lista</a>
		</div>
	<%}%>
	<div class="alert alert-dark" role="alert">
  		<h1>Categor�a:</h1> <%=dtl.getCategoria() %>
	</div>
	<%}catch (Exception e) {
		e.printStackTrace();
	  }
	%>
<jsp:include page="footer.jsp"/> 
</body>
</html>