<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtVideo" %>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="ISO-8859-1">
<title>Consulta Video</title>
 <jsp:include page="header.jsp"/>
 
 <style type="text/css">
	
	iframe {
	width: 10%;
	height: 60%;
	max-height: 20px;
	min-height: 80px;
	}
	
</style>

</head>
<body>
<div class="list-group" style="margin-top: 150px; margin-left: 50px; margin-right: 50px;">
	<%
	try{
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();

		String[] users = port.listaUsuario();

		int i = 0;
			for(int j=0;j<users.length;j++){
				String[] videos = port.listarVideos(users[j]);
				
				for(int k=0;k<videos.length;k++){
					String dv = port.mostrarDuenioVideo(videos[k]);
						if(dv.contentEquals(users[j])){
							DtVideo dtv=port.seleccionarVideo(videos[k], dv);
									%>
							<a href="VerVideo.jsp?id=<%=videos[k]%>&id2=<%=dv%>" class="list-group-item list-group-item-primary" type="submit"><iframe class="embed-responsive-item" src="<%=dtv.getURL()%>"></iframe><%=videos[k]%> (<%=dv%>)</a>
						
						<%}
						i++;
					}

			}

	}catch(Exception e){
		e.printStackTrace();
	}%>
	
</div>

<jsp:include page="footer.jsp" />
</body>
</html>