<%@page import="publicadores.DtVideo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="publicadores.Like"%>
<%@page import="publicadores.DtComentario"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtUsuario" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Ver Video</title>
	<jsp:include page="header.jsp" />
	<style >
	body{margin-top:20px;}

.comment-wrapper .panel-body {
    max-height:650px;
    overflow:auto;
}

.comment-wrapper .media-list .media img {
    width:64px;
    height:64px;
    border:2px solid #e5e7e8;
}

.comment-wrapper .media-list .media {
    border-bottom:1px dashed #efefef;
    margin-bottom:25px;
}
	/**
 * Oscuro: #283035
 * Azul: #03658c
 * Detalle: #c7cacb
 * Fondo: #dee1e3
 ----------------------------------*/
 * {
 	margin: 0;
 	padding: 0;
 	-webkit-box-sizing: border-box;
 	-moz-box-sizing: border-box;
 	box-sizing: border-box;
 }

 a {
 	color: #03658c;
 	text-decoration: none;
 }

ul {
	list-style-type: none;
}

body {
	font-family: 'Roboto', Arial, Helvetica, Sans-serif, Verdana;
	background: #dee1e3;
}

/** ====================
 * Lista de Comentarios
 =======================*/
.comments-container {
	margin: 60px auto 25px;
	width: 768px;
}

.comments-container h1 {
	font-size: 36px;
	color: #283035;
	font-weight: 400;
}

.comments-container h1 a {
	font-size: 18px;
	font-weight: 700;
}
/* este cambia el input*/
.comments-list {
	padding-right: 40%;
	margin-top: 30px;
	position: relative;
}

/**
 * Lineas / Detalles
 -----------------------*/
.comments-list:before {
	content: '';
	width: 2px;
	height: 100%;
	background: #c7cacb;
	position: absolute;
	left: 32px;
	top: 0;
}

.comments-list:after {
	content: '';
	position: absolute;
	background: #c7cacb;
	bottom: 0;
	left: 27px;
	width: 7px;
	height: 7px;
	border: 3px solid #dee1e3;
	-webkit-border-radius: 50%;
	-moz-border-radius: 50%;
	border-radius: 50%;
}




.comments-list li {
	margin-bottom: 15px;
	display: block;
	position: relative;
}

.comments-list li:after {
	content: '';
	display: block;
	clear: both;
	height: 0;
	width: 0;
}
/*esta cambia la respuesta */
.reply-list {
	padding-left: 0px;
	padding-right: 20%;
	clear: both;
	margin-top: 15px;
}
/**
 * Avatar
 ---------------------------*/
.comments-list .comment-avatar {
	width: 65px;
	height: 65px;
	position: relative;
	z-index: 99;
	float: left;
	border: 3px solid #FFF;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	overflow: hidden;
}

.comments-list .comment-avatar img {
	width: 100%;
	height: 100%;
}

.reply-list .comment-avatar {
	width: 50px;
	height: 50px;
}

.comment-main-level:after {
	content: '';
	width: 0;
	height: 0;
	display: block;
	clear: both;
}
/**
 * Caja del Comentario
 ---------------------------*/
.comments-list .comment-box {
	width: 60%;
	float: middle;
	position: relative;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
	-moz-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
	box-shadow: 0 1px 1px rgba(0,0,0,0.15);
}

.comments-list .comment-box:before, .comments-list .comment-box:after {
	content: '';
	height: 0;
	width: 0;
	position: absolute;
	display: block;
	border-width: 10px 12px 10px 0;
	border-style: solid;
	border-color: transparent #FCFCFC;
	top: 8px;
	left: -11px;
}

.comments-list .comment-box:before {
	border-width: 11px 13px 11px 0;
	border-color: transparent rgba(0,0,0,0.05);
	left: -12px;
}

.reply-list .comment-box {
	width: 610px;
}
.comment-box .comment-head {
	background: #FCFCFC;
	padding: 10px 12px;
	border-bottom: 1px solid #E5E5E5;
	overflow: hidden;
	-webkit-border-radius: 4px 4px 0 0;
	-moz-border-radius: 4px 4px 0 0;
	border-radius: 4px 4px 0 0; 
}

.comment-box .comment-head i {
	float: right;
	margin-left: 14px;
	position: relative;
	top: 2px;
	color: #A6A6A6;
	cursor: pointer;
	-webkit-transition: color 0.3s ease;
	-o-transition: color 0.3s ease;
	transition: color 0.3s ease;
}

.comment-box .comment-head i:hover {
	color: #03658c;
}

.comment-box .comment-name {
	color: #283035;
	font-size: 14px;
	font-weight: 700;
	float: left;
	margin-right: 10px;
}

.comment-box .comment-name a {
	color: #283035;
}

.comment-box .comment-head span {
	float: left;
	color: #999;
	font-size: 13px;
	position: relative;
	top: 1px;
}

.comment-box .comment-content {
	/*float: left;*/ /*  Agregado */
	background: #FFF;
	padding: 12px;
	font-size: 15px;
	color: #595959;
	-webkit-border-radius: 0 0 4px 4px;
	-moz-border-radius: 0 0 4px 4px;
	border-radius: 0 0 4px 4px;
}

.comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {color: #03658c;}
.comment-box .comment-name.by-author:after {
	content: 'autor';
	background: #03658c;
	color: #FFF;
	font-size: 12px;
	padding: 3px 5px;
	font-weight: 700;
	margin-left: 10px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

/** =====================
 * Responsive
 ========================*/
@media only screen and (max-width: 766px) {
	.comments-container {
		width: 480px;
	}

	.comments-list .comment-box {
		width: 390px;
	}

	.reply-list .comment-box {
		width: 320px;
	}
}
	</style>
</head>
	<br>
	<br>
	<%String video=request.getParameter("id");%>
	<%String usuario=request.getParameter("id2");%>
	<%try {
		HttpSession htps = request.getSession();
		String usuarioCuenta=(String)htps.getAttribute("mailu");
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		
		publicadores.DtVideo dtv =port.seleccionarVideo(video, usuario);
		
		
		if(usuarioCuenta!=null){
			try{
				if(port.seleccionarListaDeReproduccion("historial", usuarioCuenta)!=null){
					port.agregarVideoAListaReproduccion(usuario, usuarioCuenta, video, "historial");
				}
			}catch (Exception e) {
			    e.printStackTrace();
			}
		}
	%>
	
	<div class="container">
	
	 16:9 aspect ratio 
	 	<div class="embed-responsive embed-responsive-16by9">
		  <iframe class="embed-responsive-item" allowfullscreen="" src="<%=dtv.getURL()%>"></iframe>
		</div> 
		
	<hr style="width: 100%; color: red; height: 1px; background-color:black;" />
		
		
		<ul class="list-group">
		  <li class="list-group-item list-group-item-success">Nombre : <%=dtv.getNombre()%> </li>
		  <li class="list-group-item list-group-item-info">Descripcion : <%=dtv.getDescripcion()%> </li>
		  <li class="list-group-item list-group-item-danger">URL: <%=dtv.getURL()%></li>
		</ul> 
	</div>
<!-- 	------------------------------------------------------------ VALORACIONES------------------------------------------------------------------------------------------------ -->
	<%int contLike=0,contDislike=0;
	String likeUsuario;
	int valoracion=0;
	String[] listaLikes=port.listaLike(video);
	System.out.println("ESTOY ACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5");
	for(int i=0;i<(listaLikes.length);i++){
		if(usuarioCuenta!=null){
			if(listaLikes[i].contentEquals(usuarioCuenta)){
				likeUsuario=listaLikes[i];
				valoracion=2;
			}
			contLike++;
		}
	}
	
	

	
 	String[] listaDisLikes=port.listaDislike(video);
 	
	for(int i=0;i<(listaDisLikes.length);i++){
		if(usuarioCuenta!=null){
			if(listaDisLikes[i].contentEquals(usuarioCuenta)){
				likeUsuario=listaDisLikes[i];
				valoracion=1;
			}
			contDislike++;
		}
	} 



	
	
	if(valoracion==2){%>
		<h1>Like: <%=contLike %> (<%=usuarioCuenta %>)Dislikes: <%=contDislike %></h1>
	<%}else if(valoracion==1){ %>
		<h1>Like: <%=contLike %> Dislikes: <%=contDislike %> (<%=usuarioCuenta %>)</h1>
	<%}else{ %>
		<h1>Like: <%=contLike %> Dislikes: <%=contDislike %></h1>
	<%} %>
	<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="ValorarLike" method="POST">
                    <input type="hidden" value="Like" id="Like" name="Like">
                    <input type="hidden" value="<%=video %>" id="Video" name="Video">
                     <input type="hidden" value="<%=usuario %>" id="Usuario" name="Usuario">
                    <button class="btn btn-outline-secondary" type="submit">
                		 Like</button>
            </form>

            <form action="ValorarLike" method="POST">
                    <input type="hidden" value="Dislike" id="Dislike" name="Dislike">
                    <input type="hidden" value="<%=video %>" id="Video" name="Video">
                     <input type="hidden" value="<%=usuario %>" id="Usuario" name="Usuario">
                    <button class="btn btn-outline-secondary" type="submit">
                		 Dislike</button>
                     
            </form>       
          </div>
     </div>
<!-- 	------------------------------------------------------------ COMENTARIOS------------------------------------------------------------------------------------------------ -->     
	
	<form action="Comentar" method="POST">
		<div class="row bootstrap snippets">
    		<div class="col-md-6 col-md-offset-2 col-sm-12">
        		<div class="comment-wrapper">
            		<div class="panel panel-info">
		                <div class="panel-heading">
		                   
		                </div>
		                <%
		                String logeado = (String)htps.getAttribute("mailu");
		                if(logeado==null){%>
		                	<p>Debes iniciar sesion para comentar. <a href="iniciarSesion.jsp"> Hazlo Aqui.</a></p>
		                <%}%>
		                <%if(logeado!=null){%>
		                	<div class="panel-body">
                    		<input type="text-area" class="form-control" id="comentario" name="comentario" placeholder="comenta" rows="3"></input>
               
                    		<br>
                   			<button class="btn btn-info pull-right" type="submit">Comentar</button>
               		  		<input type="hidden" value="<%=video %>" id="Video" name="Video">
               		    	<input type="hidden" value="<%=usuario %>" id="Usuario" name="Usuario">	
		                    <div class="clearfix"></div>
		                    <hr>
		                </div>
		               <% }%>
		                
                		
		            </div>
		        </div>
		    </div>
		</div>
	</form>
	<!-- Contenedor Principal -->
	<div class="comments-container">
		<h1 style="color: white;">Comentarios</h1>	
	
	<%
	 DtComentario[] listadtc;
	 listadtc=port.seleccionarVideoComentario(video);
	 if(listadtc.length>0) {
		for(int i=0;i<(listadtc.length);i++) {
			Calendar cal=listadtc[i].getFecha();
			Date fechacom=cal.getTime();
			
			%>		
			<ul id="comments-list" class="comments-list">
				<li>
					<div class="comment-main-level">
					<%try{%>
						
						<div class="comment-avatar"><img src="<%=port.buscarUsuario(listadtc[i].getUsuario()).getImagen()%>" alt=""></div>
					<%}catch(Exception e){%>
						<div class="comment-avatar"><img src="#" alt=""></div>
					<%}%>
						<div class="comment-box">
							<div class="comment-head">
								<%try{
									
									DtUsuario dtuu = port.buscarUsuario(listadtc[i].getUsuario());%>
									<h6 class="comment-name by-author"><a href="#"><%=listadtc[i].getUsuario()%></a></h6>	
								<%}catch(Exception e){%>
									<h6 class="comment-name by-author"><a href="#">(Usuario Eliminado)</a></h6>
								<%}%>
								
								<span><%=fechacom%></span>
								<i class="fa fa-reply"></i>
								<i class="fa fa-heart"></i>
							</div>
							<div class="comment-content">
								<%=listadtc[i].getTexto()%>
							</div>
						</div>
					</div>
				</li>
				<div class="row bootstrap snippets">
		    		<div class="col-md-6 col-md-offset-2 col-sm-12">
		        		<div class="comment-wrapper">
		            		<div class="panel panel-info" style="margin-left: 100px;">
			            		<form action="ComentarRespuesta" method="POST">
					                <div class="panel-heading" style="color: white;">
					                    Escribe una respuesta:
					                </div>
			                		<div class="panel-body">
			                    		<input type="text-area" class="form-control" id="comentario" name="comentario" placeholder="comenta" rows="3"></input>
			                    		<br>
			                    		
			                   			<button class="btn btn-info pull-right" type="submit">Comentar</button>
			               		  		<input type="hidden" value="<%=video %>" id="Video" name="Video">
			               		    	<input type="hidden" value="<%=usuario %>" id="Usuario" name="Usuario">
			               		    	<input type="hidden" value="<%=listadtc[i].getIdComentario()%>" id="IdComentario" name="IdComentario">		
					                    <div class="clearfix"></div>
					                    <hr>
					                </div>
					              </form>
				            </div>
				        </div>
				    </div>
				</div>
			<%
			if(listadtc[i].getCantRespuestas()!=0) {
				port.seleccionarVideo2(video);
				DtComentario dtcc= listadtc[i];
				
				
				DtComentario[] dtcomen;
				
				dtcomen=port.retornarRespuestas(dtcc.getIdComentario());
				if(dtcomen.length>0){
					for(int j=0;j<dtcomen.length;j++){
						htps.setAttribute("dtc", dtcomen[j]);
						htps.setAttribute("video", video);
						htps.setAttribute("usuario", usuario);
						htps.setAttribute("usuariobase", dtcc.getUsuario());
						%>
							<jsp:include page="respuesta.jsp"/>
						<%
					}
				}
			}
			%>
			</ul>
			<%
		}
	 }else {
		 System.out.println("Lista vacia");
	 } 
	
	 %>
	</div>

		<!-- <ul id="comments-list" class="comments-list">
			<li>
				<div class="comment-main-level">
					Avatar
					<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
					Contenedor del Comentario
					<div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name by-author"><a href="http://creaticode.com/blog">Agustin Ortiz</a></h6>
							<span>hace 20 minutos</span>
							<i class="fa fa-reply"></i>
							<i class="fa fa-heart"></i>
						</div>
						<div class="comment-content">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
						</div>
					</div>
				</div>
				Respuestas de los comentarios
				<ul class="comments-list reply-list">
					<li>
						Avatar
						<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
						Contenedor del Comentario
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name"><a href="http://creaticode.com/blog">Lorena Rojero</a></h6>
								<span>hace 10 minutos</span>
								<i class="fa fa-reply"></i>
								<i class="fa fa-heart"></i>
							</div>
							<div class="comment-content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
							</div>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div> -->
<%} catch (Exception e) {
	    e.printStackTrace();
}%>
	 
	<jsp:include page="footer.jsp" />
</body>
</html>