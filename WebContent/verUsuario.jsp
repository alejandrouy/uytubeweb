<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="publicadores.DtUsuario"%>
<%@page import="publicadores.DtCanal"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="java.text.SimpleDateFormat" %>
\
<!DOCTYPE html>
<html>
<head>

<jsp:include page="header.jsp"/>
<meta charset="UTF-8">
<title>Perfil</title>

<style>
body{margin-top:20px;}

.align-center, .center {
    text-align: center!important;
}

.profile-user-info {
    display: table;
    width: 98%;
    width: calc(100% - 24px);
    margin: 0 auto
}

.profile-info-row {
    display: table-row
}

.profile-info-name,
.profile-info-value {
    display: table-cell;
    border-top: 1px dotted #D5E4F1
}

.profile-info-name {
    text-align: right;
    padding: 6px 10px 6px 4px;
    font-weight: 400;
    color: #667E99;
    background-color: transparent;
    width: 110px;
    vertical-align: middle
}

.profile-info-value {
    padding: 6px 4px 6px 6px
}

.profile-info-value>span+span:before {
    display: inline;
    content: ",";
    margin-left: 1px;
    margin-right: 3px;
    color: #666;
    border-bottom: 1px solid #FFF
}

.profile-info-value>span+span.editable-container:before {
    display: none
}

.profile-info-row:first-child .profile-info-name,
.profile-info-row:first-child .profile-info-value {
    border-top: none
}

.profile-user-info-striped {
    border: 1px solid #DCEBF7
}

.profile-user-info-striped .profile-info-name {
    color: #336199;
    background-color: #EDF3F4;
    border-top: 1px solid #F7FBFF
}

.profile-user-info-striped .profile-info-value {
    border-top: 1px dotted #DCEBF7;
    padding-left: 12px
}

.profile-picture {
    border: 1px solid #CCC;
    background-color: #FFF;
    padding: 4px;
    display: inline-block;
    max-width: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    box-shadow: 1px 1px 1px rgba(0, 0, 0, .15)
}

.dd-empty,
.dd-handle,
.dd-placeholder,
.dd2-content {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box
}

div.resp {
	width: 96%;
max-width: 960px;
margin: 0 auto;
}
img {
width: 100%;
height: auto;
}

</style>
</head>
<body>
<% 

String nick = request.getParameter("id");
HttpSession ss= request.getSession(true);
String user = (String) ss.getAttribute("mailu");

try {
	ControladorPublishService cps = new ControladorPublishServiceLocator();
	ControladorPublish port = cps.getControladorPublishPort();

	
	DtUsuario dtu = null;
	if(user==null){
		dtu = port.buscarUsuario(nick);
	}
	else{
		if(nick.contentEquals(user)){	
			dtu = port.buscarUsuario(user);	
		}
		else if(!nick.contentEquals(user)){
			dtu = port.buscarUsuario(nick);
		}
	}
	DtCanal dtc=port.retornarDtCanal();
	Date date= dtu.getFechaNac().getTime();
	String sdf= new SimpleDateFormat("dd-MM-yyyy").format(date);
	%>
	<div id="user-profile-2" class="user-profile">
			<div class="tabbable">
				
				<div class="space space-4" margin-left: 90px;>
					<%if(user!=null){ %>
						<%if(nick.contentEquals(user)){ %>
						<a href="modificarDatos.jsp" class="btn btn-primary">
							<i class="ace-icon fa fa-plus-circle bigger-120"></i>
							<span class="bigger-110">Editar Perfil</span>
						</a>
						<%} %>
					<%} %>
					<a href="verSeguidores.jsp?nick=<%=nick%>" class="btn btn-success">
						<i class="ace-icon fa fa-envelope-o bigger-110"></i>
						<span class="bigger-110">Ver Seguidores/Seguidos</span>
					</a>
				</div>
				<div class="tab-content no-border padding-24">
					<div id="home" class="tab-pane in active" align="center">
						<div class="row" align="center">
							<div class="col-xs-12 col-sm-9">
								<div class="card" style="width: 18rem;">
									<img src="<%= dtu.getImagen() %>" class="card-img-top" alt="...">
								</div>
								<h1 class="blue">
										<span class="middle" style="color: skyblue;" ><%=nick %></span>
								</h1>
							</div>
							
								<div class="profile-user-info">
									<div class="profile-info-row">
										<div class="profile-info-name"> Nickname </div>

										<div class="profile-info-value">
											<span><%=dtu.getNickname() %></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Email </div>

										<div class="profile-info-value">
											<i class="fa fa-map-marker light-orange bigger-110"></i>
											<span><%=dtu.getEmail()%></span>
											
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Nombre </div>

										<div class="profile-info-value">
											<span><%=dtu.getNombre() %></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Apellido </div>

										<div class="profile-info-value">
											<span><%=dtu.getApellido() %></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Nombre Canal </div>

										<div class="profile-info-value">
											<span><%=dtc.getNombre() %></span>
										</div>
									</div>
									<div class="profile-info-row">
										<div class="profile-info-name"> Fecha de Nacimiento </div>

										<div class="profile-info-value">
											<span><%=sdf%></span>
										</div>
									</div>
								</div>

								<div class="hr hr-8 dotted"></div>

						</div><!-- /.row -->

						<div class="space-20"></div>

						
					</div><!-- /#home -->
					<div class="space space-4" margin-left: 90px;>
						<a href="ConsultaLista.jsp?id=<%=nick%>" class="btn btn-success">
							<i class="ace-icon fa fa-envelope-o bigger-110"></i>
							<span class="bigger-110">Listas de Reproduccion</span>
						</a>
						<a href="ConsultaVideoDeUsuario.jsp?id=<%=nick %>" class="btn btn-success">
							<i class="ace-icon fa fa-envelope-o bigger-110"></i>
							<span class="bigger-110">Videos</span>
						</a>
					</div>
					<div class="space space-4" margin-left: 90px;>
						<%if(user!=null){ %>
							<%if(nick.contentEquals(user)){ %>
							<form action="EliminarCuenta" method="post">
								<input type="hidden" value="<%=nick %>" name="nick">
								<button class="btn btn-success" type="submit">Eliminar Cuenta</button>
							</form>
							<%} %>
						<%} %>
					</div>
						<%-- <div class="resp" margin-right: 150px;>
								<span class="profile-picture">
									<img  class="responsive" alt="Avatar" id="avatar2" src="<%=dtu.getImagen()%>"  width="960" height="640">
								</span>
						</div> --%><!-- /.col -->    
								
				</div>
			</div>
		</div>
	
	
	
	
	
	
	
	
<%} catch (Exception e) {
	e.printStackTrace();
}%>






<jsp:include page="footer.jsp"/>
</body>
</html>