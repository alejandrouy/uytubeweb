<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
  <a class="navbar-brand" href="index.jsp">
	<img alt="logo" src="img/logo.png" style="height: 30px;">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
		<form action="buscarVideosListasCanales" method="post" class="form-inline my-2 my-lg-0">
		      <input type="text" name="inputObjetos" value="" class="form-control mr-sm-2" placeholder="video, lista o canal" aria-label="Search" required/>
		      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
		</form>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      <li class="nav-item">
	  <% //  <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a> %>
      </li>
      <li class="nav-item dropdown">
      <%HttpSession sesion= request.getSession(false);
        if(sesion.getAttribute("mailu")==null){ %>
        	<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuario</a>
       <%}else{ %>
       		<%String ses=(String) sesion.getAttribute("mailu"); %>
       		<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><%= ses%></a>
       <%} %>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
        <%
    	
        if(sesion.getAttribute("mailu")!=null){        
        %>
        	<%String ses=(String) sesion.getAttribute("mailu"); %>
        	<a class="dropdown-item" href="SeguirUsuario.jsp">Seguir usuario</a>
             <a class="dropdown-item" href="verUsuario.jsp?id=<%=ses%>">Mi Perfil</a>
        <%} %>
        <a class="dropdown-item" href="ConsultaUsuario.jsp">Consulta Usuario</a>
        
          
        </div>
      </li>
       </li>
       
       
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Video</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
        <%if(sesion.getAttribute("mailu")!=null){        
        %>
        	<a class="dropdown-item" href="SubirVideo.jsp">Subir Video</a>
        	<a class="dropdown-item" href="mostrarVideosUser.jsp">Editar Video</a>
         <%} %>   
         <a class="dropdown-item" href="ConsultaVideo.jsp">Consulta Video</a> 
        </div>
      </li>
      <li class="nav-item dropdown">
     <%if(sesion.getAttribute("mailu")!=null){%>  
     	<%String ses=(String) sesion.getAttribute("mailu"); %>
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lista Reproduccion</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
        	<a class="dropdown-item" href="CrearListaRep.jsp">Crear Lista de Reproduccion</a>
            <a class="dropdown-item" href="ConsultaLista.jsp?id=<%= ses%>">Mis Listas de Reproduccion</a>
        </div>
       <%} %> 
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorias</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="VerCategorias.jsp">Ver Categorias</a>
        </div>
      </li>
	
  
    </ul>
    <%if(sesion.getAttribute("mailu")==null){        
        %>
  	 <a class="nav-link" href="iniciarSesion.jsp">Iniciar Sesion <span class="sr-only">(current)</span></a>
  	 <a class="nav-link" href="registrarUsuario.jsp">Registrarse <span class="sr-only">(current)</span></a>
  	 <%}else{%>
  	 
  	  <form action="CerrarSesion" method="post" class="form-signin">  
  	   		<%boolean cerro=false;%>
  	 		 <button class="btn btn-link"type="submit" onclick="<%//request.setAttribute("cerro", cerro);%>">Cerrar Sesion</button>
  	 	</form>
  	 	
  	 <%}%>
  </div>
</nav>
<div style="text-align: center;margin-top: 50px;"></div>

<!-- <main role="main" class="container"> -->


<!-- </main>/.container -->