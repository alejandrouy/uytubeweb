<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>ModificarLista</title>
	<jsp:include page="header.jsp"/>
</head>
<body>
	<div style="margin-top: 100px; margin-left: 100px; margin-right: 100px; margin-bottom: 100px;">
		<%String nombreLista= request.getParameter("id"); %>
		<form action="ModificarLista" method="post" class="form-signin">
			<input type="hidden" name="nombreLista" id="nombreLista" value="<%=nombreLista%>">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">Privada: 
				<label class="btn btn-secondary active">
					<input type="radio" name="si" id="si" autocomplete="off"> PRIVADA
				</label>
				<label class="btn btn-secondary active">
					<input type="radio" name="no" id="no" autocomplete="off"> PUBLICA
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Modificar</button>
		</form>
	</div>
    <jsp:include page="footer.jsp"/> 
</body>
</html>