<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<!DOCTYPE html>
<html>
<head> 
	<meta charset="ISO-8859-1">
	<title>CrearLista</title>
	<jsp:include page="header.jsp"/>
</head>
<body>
	<div style="margin-top: 100px; margin-left: 100px; margin-right: 100px; margin-bottom: 100px;">
		<form action="CrearLista" method="post" class="form-signin">
			<label for="text" class="sr-only">Nombre de la Lista</label>
			 <input type="text" name="nombre" value="" class="form-control input-lg" placeholder="Nombre de la lista" required/>
			<div class="btn-group btn-group-toggle" data-toggle="buttons">Privada: 
				<label class="btn btn-secondary active">
					<input type="radio" name="si" id="si" autocomplete="off"> PRIVADA
				</label>
				<label class="btn btn-secondary active">
					<input type="radio" name="no" id="no" autocomplete="off"> PUBLICA
				</label>
			</div>
			<div class="list-group" style="margin-top: 50px;">
				<%try{ 
					ControladorPublishService cps = new ControladorPublishServiceLocator();
					ControladorPublish port = cps.getControladorPublishPort();
					String[] cat = port.listaDeCategorias();
					if(cat!=null){%>
						<p>Categoria: </p>
						<select name="categoria">
						<%for(int i=0;i<cat.length;i++){%>	  
							<option value= "<%=cat[i] %>" class="list-group-item list-group-item-primary"><%=cat[i] %></option>
						<%}%>
						</select>
					<%} 
				}catch (Exception e) {
					e.printStackTrace();
	  			}%>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Crear</button>
		</form>
	</div>
    <jsp:include page="footer.jsp"/> 
</body>
</html>