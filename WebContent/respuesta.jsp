<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="publicadores.DtComentario"%>
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="publicadores.DtUsuario" %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
</head>
<body>
	<%
		System.out.println("ENTREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
		HttpSession ses=request.getSession();
				
		String video= (String) ses.getAttribute("video");
		String usuario= (String) ses.getAttribute("usuario");
		String usuariobase= (String) ses.getAttribute("usuariobase");
		try {
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			DtComentario dtc= (DtComentario) ses.getAttribute("dtc");
			port.seleccionarVideo(video, usuario);
			port.seleccionarVideoConDuenio(video, usuario);
			Calendar cal=dtc.getFecha();
			Date fechacom=cal.getTime();
			if(dtc!=null){
			%>
						<ul class="comments-list reply-list">
							<li margin-left="150px">
							<%
							try{%>
								<div class="comment-avatar"><img src="<%=port.buscarUsuario(dtc.getUsuario()).getImagen()%>" alt=""></div>								
							<%}catch(Exception e){%>
								<div class="comment-avatar"><img src="#" alt=""></div>
							<%}%>
							
								<div class="comment-box">
									<div class="comment-head">
									<%try{
										DtUsuario dtuu = port.buscarUsuario(dtc.getUsuario());%>
										<h6 class="comment-name"><a href="#"><%=dtuu.getNickname()%> - (En respuesta a <%=usuariobase%>)</a></h6>
									<%}catch(Exception e){%>
										<h6 class="comment-name"><a href="#">(Usuario Eliminado)</a></h6>
									<%} %>
										<span><%=fechacom%></span>
										<i class="fa fa-reply"></i>
										<i class="fa fa-heart"></i>
									</div>
									<div class="comment-content">
										<%=dtc.getTexto()%>
									</div>
								</div>
							</li>
							<div align="center">
								<form action="ComentarRespuesta" method="POST">
									<div class="row bootstrap snippets">
							    		<div class="col-md-6 col-md-offset-2 col-sm-12">
							        		<div class="comment-wrapper" style="margin-left: 60px;">
							            		<div class="panel panel-info">
									                <div class="panel-heading" style="color: white;">
									                    Escribe una respuesta:
									                </div>
							                		<div class="panel-body">
							                    		<input type="text-area" class="form-control" id="comentario" name="comentario" placeholder="comenta" rows="3"></input>
							                    		<br>
							                   			<button class="btn btn-info pull-right" type="submit">Comentar</button>
							               		  		<input type="hidden" value="<%=video %>" id="Video" name="Video">
							               		    	<input type="hidden" value="<%=usuario %>" id="Usuario" name="Usuario">
							               		    	<input type="hidden" value="<%=dtc.getIdComentario() %>" id="IdComentario" name="IdComentario">		
									                    <div class="clearfix"></div>
									                    <hr>
									                </div>
									            </div>
									        </div>
									    </div>
									</div>
								</form>
							</div>
						<%if(dtc.getCantRespuestas()!=0){ 
							DtComentario[] listadtc=port.retornarRespuestas(dtc.getIdComentario());
							if(listadtc.length>0)
								 for(int i=0;i<(listadtc.length);i++){
									 ses.setAttribute("dtc", listadtc[i]);
									 ses.setAttribute("usuariobase", listadtc[i].getUsuario());
									 %>
									 	<jsp:include page="respuesta.jsp"/>
									 <%
								 }
							}
						}
						%>
						</ul>
					<%} catch (Exception ioe) {
	                		ioe.printStackTrace(); 
	            	}%>
				
</body>
</html>