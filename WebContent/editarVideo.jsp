<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtVideo"%>
<%@page import="publicadores.DtUsuario"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="header.jsp"/>
<meta charset="UTF-8">
<title>Editar Video</title>
</head>
<body>
<% 
HttpSession ss= request.getSession(true);
String user = (String) ss.getAttribute("mailu");
String nombreVideo = request.getParameter("id");


try{
	ControladorPublishService cps = new ControladorPublishServiceLocator();
	ControladorPublish port = cps.getControladorPublishPort();
	
	DtVideo dtv = port.seleccionarVideo(nombreVideo,user);
	
	String codigoYT = ("https://www.youtube.com/watch?v=" + dtv.getURL().substring(30,41));
	
	%>
	<div class="container" id="wrap">
		  <div class="row">
	        <div class="col-md-6 col-md-offset-3">
	            <form action="EditarVideo" method="POST" accept-charset="utf-8" class="form" role="form">  
	  			<legend>Editar Video</legend>
	        			<input type="hidden" name="nombrevideo" value="<%=dtv.getNombre() %>" class="form-control input-lg" />
	           			<label>Nombre</label>
	                   	<input type="text" name="nombre" value="<%=dtv.getNombre() %>" class="form-control input-lg" placeholder="<%=dtv.getNombre() %>" />
	              		<label>Descripcion</label>
	                    <input type="text" name="descripcion" value="<%=dtv.getDescripcion() %>" class="form-control input-lg" placeholder="<%=dtv.getDescripcion() %>"  />
	                    <label>URL</label>
	                    <input type="text" name="url" value="<%=codigoYT %>" class="form-control input-lg" placeholder="<%=codigoYT %>" required />
			            <label>Cateogoria del Video</label>
	                    <%-- <input type="text" name="cate" value="" class="form-control input-lg" placeholder="<%=dtv.getCategoria() %>"/> --%>
	                    <h6>Categoria Actual: <%=dtv.getCategoria() %></h6>
			            
			            <label>Categoria</label>
							<%
							String[] listaCategorias = port.listaDeCategorias();
							if(listaCategorias!=null){%>
								<select name="categoria">
								<%for(int i=0;i<listaCategorias.length;i++){
								%>	  
									<option value= "<%=listaCategorias[i] %>" class="list-group-item list-group-item-primary"><%=listaCategorias[i] %></option>
								<%}%>
								</select>
							<%} %>
	
			            <input type="radio" name="publico" value="Publico">Publico
			            <span></span>
			            <input type="radio" name="publico" value="Privado">Privado
	                    
	                    <br />
	                    <button class="btn btn-secondary btn-lg btn-block" type="submit">Guardar Cambios</button>
	            </form>          
	          </div>
	      </div>
	  </div>
<%
  }catch(Exception e){
	e.printStackTrace();
}%>

<jsp:include page="footer.jsp"/>
</body>
</html>