<!doctype html>
<html lang="en">
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<%@page import="publicadores.DtUsuario"%>
<%@page import="publicadores.DtCanal"%>
<%@page import="publicadores.DtListaDeReproduccion"%>
<%@page import="publicadores.DtVideo"%>
  <head>
    <!-- Required meta tags -->
    <jsp:include page="header.jsp"/>
    
    <title>Busqueda</title>
  </head>
  <body>
<!--   <p>Si su b�squeda no es exitosa, puede ser porque el elemento que busca no es publico hacia todos.</p> -->
	<br>
	<br>
	<div class="container">
	  	<div class="list-group">
			<%try{ 
				HttpSession ses = request.getSession(false);
			  	if(ses!=null){
					ControladorPublishService cps = new ControladorPublishServiceLocator();
					ControladorPublish port = cps.getControladorPublishPort();
				  	HttpSession hs= request.getSession(false);
					String seleccionado=request.getParameter("id");
					
					if(seleccionado!=null){ %>
						<p>Resultado de la busqueda: <%=seleccionado %></p>
						
						<%String[] listaUsuarios= port.listaUsuario();
						
						for(int u=0;u<listaUsuarios.length;u++){
							
							DtUsuario dtu=port.buscarUsuario(listaUsuarios[u]);
						
							if(port.retornarDtCanal().getNombre().contentEquals(seleccionado)){
								
								DtCanal dtc=port.retornarDtCanal();
								
								if(dtc.isPrivado()==false){%>
								
									<a href="verUsuario.jsp?id=<%=dtu.getNickname() %>" class="list-group-item list-group-item-action">
									<div class="d-flex w-100 justify-content-between">
									    <h5 class="mb-1">Canal</h5>
									    <small><%=dtc.getNombre()%></small>
									</div>
										  	<p class="mb-1"><%=dtc.getDescripcion() %></p>
										<small><%if(dtc.isPrivado()){ %>Privado <%}else{ %>Publico<%} %></small>
									</a>
								<%}
								
						 	}%>
							<%
							try{
								DtListaDeReproduccion dtl=port.seleccionarListaDeReproduccion(seleccionado, listaUsuarios[u]);
								
								if(dtl!=null){
									
									if(dtl.isPrivado()==false){
										String duenioLista = port.mostrarDuenioLista(seleccionado);%>
										
										<a href="ListaDeReproduccion.jsp?id=<%=seleccionado %>&id2=<%=duenioLista %>" class="list-group-item list-group-item-action">
										<div class="d-flex w-100 justify-content-between">
										    <h5 class="mb-1">Lista de Reproduccion</h5>
										    <small><%=listaUsuarios[u] %></small>
										</div>
										 	  <p class="mb-1"><%=dtl.getNombre()%></p>
										 <small><%=dtl.getCategoria() %></small>
										</a>
									<%}
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							%>
							<%
							try{
								DtVideo dtv=port.seleccionarVideo(seleccionado, listaUsuarios[u]);
							
								if(dtv!=null){
	
									if(dtv.isPrivado()==false){%>
									
										<a href="VerVideo.jsp?id=<%=seleccionado %>&id2=<%=listaUsuarios[u] %>" class="list-group-item list-group-item-action">
										<div class="d-flex w-100 justify-content-between">
										    <h5 class="mb-1">Video</h5>
										    <small><%=listaUsuarios[u] %></small>
										</div>
										 	<p class="mb-1"><%=dtv.getNombre()%></p>
											<small><%=dtv.getCategoria() %></small>
											<small><%=dtv.getDuracion() %></small>
										</a>
										
									<%} %>
							
								<%}%>
							<%}catch (Exception e) {
								e.printStackTrace();
							}  %>
				  	  <%}%>
				  <%}%>
				<%} %>
			<%}catch (Exception e) {
				e.printStackTrace();
			} %>
		</div>
	</div>
    <jsp:include page="footer.jsp"/> 
    
  </body>
</html>