<jsp:include page="header.jsp"/>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Iniciar Sesion</title> 
    <link href="css/iniciarSesion.css" rel="stylesheet">
  </head>
  <br>
  <br>
  <div class="wrapper fadeInDown">
	  <div id="formContent">
	    <form action="InicioSesion" method="post" class="form-signin">
	      <input type="text" id="inputEmail" class="fadeIn second" name="email" placeholder="Ingrese su Email o Nickname" required autofocus>
	      <input type="password" id="inputPassword" class="fadeIn third" name="password" placeholder="Password" required>
	      <input type="submit" class="fadeIn fourth" value="Iniciar Sesion">
	    </form>
	
	    <!-- Remind Passowrd -->
	    <div id="formFooter">
	      <a class="underlineHover" href="registrarUsuario.jsp">No tienes una cuenta?. Registrate Aqui</a>
	    </div>
	
	  </div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	</body>
</html>


