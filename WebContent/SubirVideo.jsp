<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>  
<%@page import="publicadores.ControladorPublish" %>
<%@page import="publicadores.ControladorPublishService" %>
<%@page import="publicadores.ControladorPublishServiceLocator" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Subir Video</title>
 <jsp:include page="header.jsp"/>
 
</head>

<body>
<br>
<br>
<br>
<br>
 <div class="container" id="wrap">
	  <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="SubirVideo" method="POST" accept-charset="utf-8" class="form" role="form">   <legend>Subir Video</legend>
            
                   
                    <input type="text" name="NombreVideo" value=""  placeholder="Nombre Video" required/>
                    <input type="text" name="Descripcion" value="" class="form-control input-lg" placeholder="Descripcion"  required/>
                   	<input type="text" name="URL" value="" class="form-control input-lg" placeholder="URL"  required/>
                   	<div class="btn-group btn-group-toggle" data-toggle="buttons"> 
						<label class="btn btn-secondary active">
							<input type="radio" name="si" id="si" autocomplete="off"> PRIVADO
						</label>
						<label class="btn btn-secondary active">
							<input type="radio" name="no" id="no" autocomplete="off"> PUBLICO
						</label>
					</div>
                   	<div class="list-group" style="margin-top: 10px;margin-bottom: 10px">
				<%try {
				ControladorPublishService cps = new ControladorPublishServiceLocator();
				ControladorPublish port = cps.getControladorPublishPort();
				int cont=0;
				if(port.listaDeCategorias().length!=0){%>
					<p>Categoria: </p>
					<select name="categoria">
					<%String[] listacat=port.listaDeCategorias(); %>
					<%for(int i=0;i<port.listaDeCategorias().length;i++){%>	  
					
						<option value= "<%=listacat[i]%>" class="list-group-item list-group-item-primary"><%=listacat[i]%></option>
		
					<%}%>
					</select>
				<%}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>                    
                    <br />
                    <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">
                		 Subir Video</button>
       				</div>
       			</form>	                    
           </div>
      </div>  
</div>      


<jsp:include page="footer.jsp"/> 

</body>
</html>